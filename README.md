# Управление продуктами и пользователями

Данное приложение разработано на Spring Boot версии 3.1.5 и Java 17. Оно предоставляет возможность управления продуктами и пользователями. Проект использует следующие зависимости:
- Spring Boot Starter Data JPA
- Spring Boot Starter Web
- Mariadb
- Lombok
- Spring Boot Starter Test
- Spring Security

## Эндпоинты
Ниже перечислены доступные эндпоинты в приложении:

### GET /
Данный эндпоинт позволяет получить публичную информацию без необходимости аутентификации.
```java
@GetMapping("/")
public String goHome(){
    return "This is publickly accesible withing needing authentication";
}
```

### POST /user/save
Этот эндпоинт используется для сохранения нового пользователя. Пароль пользователя будет зашифрован перед сохранением в базу данных.
```java
@PostMapping("/user/save")
public ResponseEntity<Object> saveUser(@RequestBody OurUser ourUser){
    ourUser.setPassword(passwordEncoder.encode(ourUser.getPassword()));
    OurUser result = ourUserRepo.save(ourUser);
    if(result.getId()>0){
        return ResponseEntity.ok("User Was Saved");
    }
    return ResponseEntity.status(404).body("Error"+"User Not Saved");
}
```

### GET /product/all
Получение списка всех продуктов.
```java
@GetMapping("/product/all")
public ResponseEntity<Object> getAllProducts(){
    return ResponseEntity.ok(productRepo.findAll());
}
```

### GET /users/all
Возвращает список всех пользователей. Доступен только для пользователей с ролью "ADMIN".
```java
@GetMapping("/users/all")
@PreAuthorize("hasAuthority('ADMIN')")
public ResponseEntity<Object> getAllUsers(){
    return ResponseEntity.ok(ourUserRepo.findAll());
}
```

### GET /users/single
Возвращает информацию о текущем пользователе. Доступен для пользователей с ролью "ADMIN" или "USER".
```java
@GetMapping("/users/single")
@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('USER')")
public ResponseEntity<Object> getMyDetails(){
    return ResponseEntity.ok(ourUserRepo.findByEmail(getLoggedInUserDetails().getUsername()));
}
```

### Модель OurUser
```java
@Data
@Entity
@Table(name = "ourusers")
public class OurUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String email;
    private String password;
    private String roles;
}
```

### getLoggedInUserDetails()
Метод getLoggedInUserDetails() используется для получения информации о текущем аутентифицированном пользователе.
```java
public UserDetails getLoggedInUserDetails(){
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if(authentication!=null&&authentication.getPrincipal() instanceof UserDetails){
        return (UserDetails) authentication.getPrincipal();
    }
    return null;
}
```