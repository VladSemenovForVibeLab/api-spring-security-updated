package ru.semenov.spring.security.updated.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.semenov.spring.security.updated.model.OurUser;
import ru.semenov.spring.security.updated.repository.OurUserRepo;

import java.util.Optional;

@Configuration
public class OurUserInfoUserDetailsService implements UserDetailsService {
    @Autowired
    private OurUserRepo ourUserRepo;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<OurUser> user = ourUserRepo.findByEmail(username);
        return user
                .map(OurUserInfoDetails::new)
                .orElseThrow(()->new UsernameNotFoundException("User Does Not Exist"));
    }
}
