package ru.semenov.spring.security.updated.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.spring.security.updated.model.Product;

public interface ProductRepo extends JpaRepository<Product, Integer> {
}
