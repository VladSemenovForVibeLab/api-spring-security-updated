package ru.semenov.spring.security.updated.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.semenov.spring.security.updated.model.OurUser;

import java.util.Optional;

public interface OurUserRepo extends JpaRepository<OurUser,Integer> {
    @Query(value = "select * from ourusers where email = ?1",nativeQuery = true)
    Optional<OurUser> findByEmail(String email);
}
